﻿<# This form was created using POSHGUI.com  a free online gui designer for PowerShell
.NAME
    Untitled
	test line
#>
ipmo ac*
$DC = (Get-ADDomainController).Name
Add-Type -AssemblyName System.Windows.Forms
[System.Windows.Forms.Application]::EnableVisualStyles()

#region begin GUI{ 

$Form                            = New-Object system.Windows.Forms.Form
$Form.ClientSize                 = '497,300'
$Form.text                       = "USB Port Manager"
$Form.TopMost                    = $false

$TextBox1                        = New-Object system.Windows.Forms.TextBox
$TextBox1.width                  = 145
$TextBox1.height                 = 20
$TextBox1.location               = New-Object System.Drawing.Point(22,24)
$TextBox1.Font                   = 'Palatino Linotype,12'

# AutoComplete Begin Here
#$TextBox1.AutoCompleteSource      ='CustomSource'
#$TextBox1.AutoCompleteMode        ='SuggestAppend'
#$TextBox1.AutoCompleteCustomSource=$autocomplete
#(Get-ADComputer -filter * -Server $DC).Name | % {$TextBox1.AutoCompleteCustomSource.AddRange($_) }


$ListBox1                        = New-Object system.Windows.Forms.ListBox
$ListBox1.text                   = "listBox"
$ListBox1.width                  = 145
$ListBox1.height                 = 210
$ListBox1.location               = New-Object System.Drawing.Point(22,70)

$Button1                         = New-Object system.Windows.Forms.Button
$Button1.BackColor               = "#9b9b9b"
$Button1.text                    = "Search"
$Button1.width                   = 108
$Button1.height                  = 30
$Button1.location                = New-Object System.Drawing.Point(194,24)
$Button1.Font                    = 'Palatino Linotype,12'
$Button1.ForeColor               = "#ffffff"
$Button1.Enabled                 = $false

$Button2                         = New-Object system.Windows.Forms.Button
$Button2.BackColor               = "#9b9b9b"
$Button2.text                    = "Check"
$Button2.width                   = 108
$Button2.height                  = 30
$Button2.location                = New-Object System.Drawing.Point(342,24)
$Button2.Font                    = 'Palatino Linotype,12'
$Button2.ForeColor               = "#ffffff"
$Button2.Enabled                 = $false

$Button3                         = New-Object system.Windows.Forms.Button
$Button3.BackColor               = "#9b9b9b"
$Button3.text                    = "Unlock"
$Button3.width                   = 108
$Button3.height                  = 30
$Button3.location                = New-Object System.Drawing.Point(192,70)
$Button3.Font                    = 'Palatino Linotype,12'
$Button3.ForeColor               = "#ffffff"
$Button3.Enabled                 = $false

$Button5                         = New-Object system.Windows.Forms.Button
$Button5.BackColor               = "#9b9b9b"
$Button5.text                    = "Temp. Unlock"
$Button5.width                   = 108
$Button5.height                  = 30
$Button5.location                = New-Object System.Drawing.Point(192,110)
$Button5.Font                    = 'Palatino Linotype,12'
$Button5.ForeColor               = "#ffffff"
$Button5.Enabled                 = $false

$Button6                         = New-Object system.Windows.Forms.Button
$Button6.BackColor               = "#9b9b9b"
$Button6.text                    = "Lock"
$Button6.width                   = 108
$Button6.height                  = 30
$Button6.location                = New-Object System.Drawing.Point(192,150)
$Button6.Font                    = 'Palatino Linotype,12'
$Button6.ForeColor               = "#ffffff"
$Button6.Enabled                 = $false

$Button7                         = New-Object system.Windows.Forms.Button
$Button7.BackColor               = "#9b9b9b"
$Button7.text                    = "Clear"
$Button7.width                   = 108
$Button7.height                  = 30
$Button7.location                = New-Object System.Drawing.Point(192,190)
$Button7.Font                    = 'Palatino Linotype,12'
$Button7.ForeColor               = "#ffffff"
$Button7.Enabled                 = $false

$Button8                         = New-Object system.Windows.Forms.Button
$Button8.BackColor               = "#9b9b9b"
$Button8.text                    = "Exit"
$Button8.width                   = 108
$Button8.height                  = 30
$Button8.location                = New-Object System.Drawing.Point(342,240)
$Button8.Font                    = 'Palatino Linotype,12'
$Button8.ForeColor               = "#ffffff"

$Form.controls.AddRange(@($TextBox1,$ListBox1,$Button1,$Button2,$Button3,$Button5,$Button6,$Button7,$Button8))

#region gui events {

# Duymelerin enable disable edilmesi ucun ayarlar
$TextBox1.add_TextChanged({IsThereText})
function IsThereText
{
	if (($TextBox1.Text.Length -ne 0))
	{
		$Button1.Enabled = $true

	}
	else
	{
		$Button1.Enabled = $false
    }
}

$ListBox1.Add_Click({ 
    if (($ListBox1.SelectedItem.Length -ne 0 ))
    {
        $Button2.Enabled = $true
        $Button3.Enabled = $true
        $Button5.Enabled = $true
        $Button6.Enabled = $true
        $Button7.Enabled = $true
    }
    else
    {
        $Button2.Enabled = $false
        $Button3.Enabled = $false
        $Button5.Enabled = $false
        $Button6.Enabled = $false
        $Button7.Enabled = $false
     }
 })
 #Bitdi

# AXTARIW BURDAN BAWLAYIR (Search duymesi)
$Button1.Add_Click({  
$text = $TextBox1.Text
$computers = (Get-ADComputer -LdapFilter "(Name=*$text*)" -Server $DC).Name
$ListBox1.Items.Clear()
foreach ($pc in $computers)
    {
    [Void]$ListBox1.Items.Add($pc)
    }
})
# BURDA BITIR

# Check duymesi burda baslayir
$Button2.Add_Click({
$CompGroups = Get-ADComputer -Identity $ListBox1.SelectedItem -Properties MemberOF -Server $DC| %{$_.memberof} | %{get-adgroup $_ | select name}
if ($CompGroups -like "*USBUnlock*")
    {
    Add-Type -AssemblyName PresentationCore,PresentationFramework
    $ButtonType = [System.Windows.MessageBoxButton]::OK
    $MessageIcon = [System.Windows.MessageBoxImage]::Information 
    $MessageBody = "USB PORT AKTIVDIR"
    $MessageTitle = "USB"
    $Result = [System.Windows.MessageBox]::Show($MessageBody,$MessageTitle,$ButtonType,$MessageIcon)
    }
elseif ($CompGroups -like "*USBTemp*") 
    {
    Add-Type -AssemblyName PresentationCore,PresentationFramework
    $ButtonType = [System.Windows.MessageBoxButton]::OK
    $MessageIcon = [System.Windows.MessageBoxImage]::Information 
    $MessageBody = "USB PORT GUN SONUNA KIMI AKTIVDIR"
    $MessageTitle = "USB"
    $Result = [System.Windows.MessageBox]::Show($MessageBody,$MessageTitle,$ButtonType,$MessageIcon)
    }
else {
    Add-Type -AssemblyName PresentationCore,PresentationFramework
    $ButtonType = [System.Windows.MessageBoxButton]::OK
    $MessageIcon = [System.Windows.MessageBoxImage]::Information 
    $MessageBody = "USB PORT DEAKTIVDIR"
    $MessageTitle = "USB"
    $Result = [System.Windows.MessageBox]::Show($MessageBody,$MessageTitle,$ButtonType,$MessageIcon)
    }
})
# CHECK DUYMESI BURDA BITIR

# UNLOCK DUYMESI BURDA BAWLAYIR
$Button3.Add_Click({
$CompGroups = Get-ADComputer -Identity $ListBox1.SelectedItem -Properties MemberOF -Server $DC | %{$_.memberof} | %{get-adgroup $_ | select name}
if ($CompGroups -like "*USBUnlock*")
    {
    Add-Type -AssemblyName PresentationCore,PresentationFramework
    $ButtonType = [System.Windows.MessageBoxButton]::OK
    $MessageIcon = [System.Windows.MessageBoxImage]::Information 
    $MessageBody = "USB YETKISI ARTIG MOVCUDDUR"
    $MessageTitle = "USB"
    $Result = [System.Windows.MessageBox]::Show($MessageBody,$MessageTitle,$ButtonType,$MessageIcon)
    }
elseif ($CompGroups -like "*USBTemp*")
    {
    Add-Type -AssemblyName PresentationCore,PresentationFramework
    $ButtonType = [System.Windows.MessageBoxButton]::OK
    $MessageIcon = [System.Windows.MessageBoxImage]::Information 
    $MessageBody = "USB YETKISI ARTIG MOVCUDDUR"
    $MessageTitle = "USB"
    $Result = [System.Windows.MessageBox]::Show($MessageBody,$MessageTitle,$ButtonType,$MessageIcon)
    }
else
    {
    Add-ADGroupMember -Identity usbunlock -Members(Get-ADComputer $ListBox1.SelectedItem) -Server $DC
    Add-Type -AssemblyName PresentationCore,PresentationFramework
    $ButtonType = [System.Windows.MessageBoxButton]::OK
    $MessageIcon = [System.Windows.MessageBoxImage]::Information 
    $MessageBody = "USB PORT AKTIV EDILDI"
    $MessageTitle = "USB"
    $Result = [System.Windows.MessageBox]::Show($MessageBody,$MessageTitle,$ButtonType,$MessageIcon)
    }
  })
# TEMP. UNLOCK DUYMESI BURDA BITIR
$Button5.Add_Click({
$CompGroups = Get-ADComputer -Identity $ListBox1.SelectedItem -Properties MemberOF -Server $DC | %{$_.memberof} | %{get-adgroup $_ | select name}
if ($CompGroups -like "*USBUnlock*")
    {
    Add-Type -AssemblyName PresentationCore,PresentationFramework
    $ButtonType = [System.Windows.MessageBoxButton]::OK
    $MessageIcon = [System.Windows.MessageBoxImage]::Information 
    $MessageBody = "USB YETKISI ARTIG MOVCUDDUR"
    $MessageTitle = "USB"
    $Result = [System.Windows.MessageBox]::Show($MessageBody,$MessageTitle,$ButtonType,$MessageIcon)
    }
elseif ($CompGroups -like "*USBTemp*")
    {
    Add-Type -AssemblyName PresentationCore,PresentationFramework
    $ButtonType = [System.Windows.MessageBoxButton]::OK
    $MessageIcon = [System.Windows.MessageBoxImage]::Information 
    $MessageBody = "USB YETKISI ARTIG MOVCUDDUR"
    $MessageTitle = "USB"
    $Result = [System.Windows.MessageBox]::Show($MessageBody,$MessageTitle,$ButtonType,$MessageIcon)
    }
else
    {
    Add-ADGroupMember -Identity usbtemp -Members(Get-ADComputer $ListBox1.SelectedItem) -Server $DC
    Add-Type -AssemblyName PresentationCore,PresentationFramework
    $ButtonType = [System.Windows.MessageBoxButton]::OK
    $MessageIcon = [System.Windows.MessageBoxImage]::Information 
    $MessageBody = "USB PORT GUN SONUNA KIMI AKTIV EDILDI"
    $MessageTitle = "USB"
    $Result = [System.Windows.MessageBox]::Show($MessageBody,$MessageTitle,$ButtonType,$MessageIcon)
    }
})
# LOCK DUYMESI BURDA BAWLAYIR
$Button6.Add_Click({
$CompGroups = Get-ADComputer -Identity $ListBox1.SelectedItem -Properties MemberOF -Server $DC | %{$_.memberof} | %{get-adgroup $_ | select name}
if ($CompGroups -like "*USBUnlock*")
    {
    Remove-ADGroupMember -Identity USBUnlock -Members(Get-ADComputer $ListBox1.SelectedItem) -Server $DC -Confirm:$false
    Add-Type -AssemblyName PresentationCore,PresentationFramework
    $ButtonType = [System.Windows.MessageBoxButton]::OK
    $MessageIcon = [System.Windows.MessageBoxImage]::Information 
    $MessageBody = "USB YETKISI LEGV EDILDI"
    $MessageTitle = "USB"
    $Result = [System.Windows.MessageBox]::Show($MessageBody,$MessageTitle,$ButtonType,$MessageIcon)
    }
elseif ($CompGroups -like "*USBTemp*")
    {
    Remove-ADGroupMember -Identity USBTEMP -Members(Get-ADComputer $ListBox1.SelectedItem) -Server $DC -Confirm:$false
    Add-Type -AssemblyName PresentationCore,PresentationFramework
    $ButtonType = [System.Windows.MessageBoxButton]::OK
    $MessageIcon = [System.Windows.MessageBoxImage]::Information 
    $MessageBody = "MUVEQQETI USB YETKISI LEGV EDILDI"
    $MessageTitle = "USB"
    $Result = [System.Windows.MessageBox]::Show($MessageBody,$MessageTitle,$ButtonType,$MessageIcon)
    }
else
    {
    Add-Type -AssemblyName PresentationCore,PresentationFramework
    $ButtonType = [System.Windows.MessageBoxButton]::OK
    $MessageIcon = [System.Windows.MessageBoxImage]::Information 
    $MessageBody = "BU BILGISAYAR UCUN USB YETKISI ONCEDEN AKTIV DEYIL"
    $MessageTitle = "USB"
    $Result = [System.Windows.MessageBox]::Show($MessageBody,$MessageTitle,$ButtonType,$MessageIcon)
    }
  })
# LOCK DUYMESI BURDA BITIR

# CLEAR DUYMESI BURDA BAWLAYIR
$Button7.Add_Click({
    $TextBox1.Clear()
    $ListBox1.Items.Clear()
    $Button2.Enabled = $false
    $Button3.Enabled = $false
    $Button5.Enabled = $false
    $Button6.Enabled = $false
    $Button7.Enabled = $false
  })
# CLEAR DUYMESI BURDA BITIR

$Button8.Add_Click({
$Form.Close()
  })


#endregion events }

#endregion GUI }




[void]$Form.ShowDialog()